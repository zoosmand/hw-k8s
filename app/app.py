from flask import Flask
import netifaces
import os

app = Flask(__name__)


@app.route('/')
def hello():
    ifs = []
    for interface in netifaces.interfaces():
        try:
            ifs.append((interface, netifaces.ifaddresses(interface)[netifaces.AF_INET][0]["addr"]))
        except KeyError:
            pass

    return f"Hello, World!!! Version: {os.getenv('APP_VERSION')} {ifs}"
