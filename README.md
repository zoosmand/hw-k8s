# The small K8S application deployment


## Before all, the image must be built and pushed to a registry
(The registryy authethnication and authorization staff is out of the field)

~~~ bash
cd ./app
export APP_VERSION=0.0.1
docker buildx build -t app:$APP_VERSION . > .log 2>&1

docker push app:$APP_VERSION
~~~


## Fix line 25 in hello-world.yml according to the existing registry/image


## Deploy app

~~~ bash
kubectl apply -f ./hello-world.yml
~~~

## Change the number of replicas in the hello-world.yml and apply the deployment

(Look at the changes <http://localhost:30001>)

~~~ bash
watch kubectl get all -n default
~~~

## Change the line 3 in ./app/Dockerfile. Up the version. Push the new image. Apply the deployment

(Look at the changes <http://localhost:30001>)
